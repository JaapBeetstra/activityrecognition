package com.app_it_up.activityrecognition;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ActivityProvider extends ContentProvider {
	private static final String AUTHORITY = "com.app_it_up.activityrecognition";
	private static final String BASE_PATH = "activities";
	public static final Uri LIST_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
	private ActivityLogDB database;

	@Override
	public boolean onCreate() {
		database = new ActivityLogDB(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(ActivityLogDB.TABLE_ACTIVITY);
		if (!uri.equals(LIST_URI))
			throw new IllegalArgumentException("Unknown URI: " + uri);
		SQLiteDatabase db = database.getReadableDatabase();
		Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		if (!uri.equals(LIST_URI))
			throw new IllegalArgumentException("Unknown URI: " + uri);
		long id = sqlDB.insert(ActivityLogDB.TABLE_ACTIVITY, null, values);
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		if (!uri.equals(LIST_URI))
			throw new IllegalArgumentException("Unknown URI: " + uri);
		int rowsDeleted = sqlDB.delete(ActivityLogDB.TABLE_ACTIVITY, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		if (!uri.equals(LIST_URI))
			throw new IllegalArgumentException("Unknown URI: " + uri);
		int rowsUpdated = sqlDB.update(ActivityLogDB.TABLE_ACTIVITY, values, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}
}
