package com.app_it_up.activityrecognition;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.ActivityRecognitionClient;

public class ActivityRecognitionHelper {
	static final String LOG_TAG = "ActivityRecognitionHelper";
	MainActivity mainActivity;
	PendingIntent activityIntent;
	ActivityRecognitionService service;
	ActivityRecognitionClient activityRecognitionClient;
	Boolean recognizing;

	public ActivityRecognitionHelper(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
	}

	public void onCreate() {
		if (activityIntent != null && activityRecognitionClient != null)
			return;
		Context context = mainActivity.getApplicationContext();
		Intent intent = new Intent(context, ActivityRecognitionService.class);
		activityIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		activityRecognitionClient = new ActivityRecognitionClient(context, connectedListener, connectionFailedListener);
	}

	ConnectionCallbacks connectedListener = new ConnectionCallbacks() {
		@Override
		public void onConnected(Bundle connectionHint) {
			mainActivity.clearBusy();
			updateState();
		}

		@Override
		public void onDisconnected() {
			mainActivity.clearBusy();
			updateState();
		}
	};
	OnConnectionFailedListener connectionFailedListener = new OnConnectionFailedListener() {
		@Override
		public void onConnectionFailed(ConnectionResult result) {
			mainActivity.clearBusy();
			updateState();
			if (result.hasResolution())
				try {
					result.startResolutionForResult(mainActivity, MainActivity.CONNECTION_FAILURE_RESOLUTION_REQUEST);
				} catch (SendIntentException e) {
					Log.e(LOG_TAG, "Problem fixing connection failure", e);
				}
			else
				showGooglePlayErrorDialog(result.getErrorCode());
		}
	};

	public void onStart() {
		if (googlePlayServicesAvailable()) {
			if (activityRecognitionClient.isConnected())
				return;
			mainActivity.setBusy();
			activityRecognitionClient.connect();
		}
	}

	public void onStop() {
		if (activityRecognitionClient.isConnected()) {
			mainActivity.setBusy();
			activityRecognitionClient.disconnect();
			mainActivity.clearBusy();
		}
		updateState();
	}

	public void startUpdates(int detectionIntervalMillis) {
		if (!activityRecognitionClient.isConnected())
			return;
		if (Boolean.TRUE == recognizing)
			return;
		mainActivity.setBusy();
		Log.i(LOG_TAG, "requestActivityUpdates");
		activityRecognitionClient.requestActivityUpdates(detectionIntervalMillis, activityIntent);
		recognizing = Boolean.TRUE;
		mainActivity.clearBusy();
		updateState();
	}

	public void stopUpdates() {
		if (!activityRecognitionClient.isConnected())
			return;
		if (Boolean.FALSE == recognizing)
			return;
		mainActivity.setBusy();
		Log.i(LOG_TAG, "removeActivityUpdates");
		activityRecognitionClient.removeActivityUpdates(activityIntent);
		recognizing = Boolean.FALSE;
		mainActivity.clearBusy();
		updateState();
	}

	void updateState() {
		mainActivity.updateState(activityRecognitionClient.isConnected(), recognizing);
	}

	boolean googlePlayServicesAvailable() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mainActivity);
		if (ConnectionResult.SUCCESS == resultCode) {
			Log.i(LOG_TAG, "Google Play services is available.");
			return true;
		} else {
			showGooglePlayErrorDialog(resultCode);
			return false;
		}
	}

	void showGooglePlayErrorDialog(int resultCode) {
		Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, mainActivity,
				MainActivity.CONNECTION_FAILURE_RESOLUTION_REQUEST);
		if (dialog != null) {
			ErrorDialogFragment errorFragment = ErrorDialogFragment.create(dialog);
			errorFragment.show(mainActivity.getFragmentManager(), "Activity Recognition");
		}
	}
}
