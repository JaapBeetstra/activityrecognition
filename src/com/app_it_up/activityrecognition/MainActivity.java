package com.app_it_up.activityrecognition;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {
	static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	static final int DETECTION_INTERVAL_MILLIS = 20 * 1000;
	static final int ACTIVITY_LOADER = 1;
	static final String LOG_TAG = "MainActivity";
	final ActivityRecognitionHelper helper = new ActivityRecognitionHelper(this);
	CursorAdapter adapter;
	int busyCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ListView listView = (ListView) findViewById(R.id.activity_list);
		adapter = new ActivityAdapter(this);
		listView.setAdapter(adapter);
		getLoaderManager().initLoader(ACTIVITY_LOADER, null, this);
		helper.onCreate();
	}

	@Override
	protected void onStart() {
		helper.onStart();
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
		helper.onStop();
	}

	@SuppressWarnings("unused")
	public void startUpdates(View view) {
		helper.startUpdates(DETECTION_INTERVAL_MILLIS);
	}

	@SuppressWarnings("unused")
	public void stopUpdates(View view) {
		helper.stopUpdates();
	}

	public void setBusy() {
		busyCount++;
		if (busyCount > 0)
			findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
	}

	public void clearBusy() {
		busyCount--;
		if (busyCount == 0)
			findViewById(R.id.progress_bar).setVisibility(View.GONE);
	}

	public void updateState(boolean connected, Boolean recognizing) {
		findViewById(R.id.start_updates_button).setEnabled(Boolean.FALSE != connected && Boolean.TRUE != recognizing);
		findViewById(R.id.stop_updates_button).setEnabled(Boolean.FALSE != connected && Boolean.FALSE != recognizing);
		TextView gpsState = (TextView) findViewById(R.id.play_services_state);
		gpsState.setText(getPlayServicesStateText(connected));
		TextView arState = (TextView) findViewById(R.id.activity_recognition_state);
		arState.setText(getActivityRecoginitionStateText(recognizing));
	}

	int getPlayServicesStateText(boolean connected) {
		if (connected)
			return R.string.play_services_state_connected;
		return R.string.play_services_state_disconnected;
	}

	int getActivityRecoginitionStateText(Boolean recognizing) {
		if (recognizing == Boolean.TRUE)
			return R.string.activity_recognition_state_active;
		if (recognizing == Boolean.FALSE)
			return R.string.activity_recognition_state_inactive;
		return R.string.activity_recognition_state_unknown;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		if (id == ACTIVITY_LOADER)
			return new CursorLoader(this, ActivityProvider.LIST_URI, ActivityLogDB.PROJECTION, null, null,
					ActivityLogDB.COLUMN_ACTIVITY_TIME + " DESC");
		throw new IllegalArgumentException("don't know " + id);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		adapter.changeCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.changeCursor(null);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
			if (resultCode == Activity.RESULT_OK) {
				helper.onStop();
			}
		} else
			super.onActivityResult(requestCode, resultCode, data);
	}
}
