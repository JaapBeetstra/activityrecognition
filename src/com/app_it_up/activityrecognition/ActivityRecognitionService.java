package com.app_it_up.activityrecognition;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

public class ActivityRecognitionService extends IntentService {
	private static final String LOG_TAG = "ActivityRecognitionService";

	public ActivityRecognitionService() {
		super("ActivityRecognitionService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (ActivityRecognitionResult.hasResult(intent)) {
			handleActivityRecognitionResult(ActivityRecognitionResult.extractResult(intent));
		}
	}

	protected void handleActivityRecognitionResult(ActivityRecognitionResult result) {
		Log.i(LOG_TAG, "handleActivityRecognitionResult: " + result);
		DetectedActivity mostProbableActivity = result.getMostProbableActivity();
		int confidence = mostProbableActivity.getConfidence();
		int activityType = mostProbableActivity.getType();
		String activityName = getNameFromType(activityType);
		String alternatives = getAlternatives(result.getProbableActivities(), activityType);

		ContentValues values = new ContentValues();
		values.put(ActivityLogDB.COLUMN_ACTIVITY_TIME, result.getTime());
		values.put(ActivityLogDB.COLUMN_ACTIVITY_TYPE, activityName);
		values.put(ActivityLogDB.COLUMN_ACTIVITY_CONFIDENCE, confidence);
		values.put(ActivityLogDB.COLUMN_ACTIVITY_ALTERNATIVES, alternatives);
		getContentResolver().insert(ActivityProvider.LIST_URI, values);
	}

	private static String getNameFromType(int activityType) {
		switch (activityType) {
		case DetectedActivity.IN_VEHICLE:
			return "in_vehicle";
		case DetectedActivity.ON_BICYCLE:
			return "on_bicycle";
		case DetectedActivity.ON_FOOT:
			return "on_foot";
		case DetectedActivity.STILL:
			return "still";
		case DetectedActivity.UNKNOWN:
			return "unknown";
		case DetectedActivity.TILTING:
			return "tilting";
		}
		return "unknown";
	}

	protected static String getAlternatives(Iterable<DetectedActivity> activities, int filtered) {
		StringBuilder sb = new StringBuilder();
		for (DetectedActivity activity : activities)
			if (activity.getType() != filtered) {
				if (sb.length() != 0)
					sb.append(", ");
				sb.append(getNameFromType(activity.getType()));
				sb.append(": ");
				sb.append(activity.getConfidence());
				sb.append("%");
			}
		return sb.toString();
	}
}
