package com.app_it_up.activityrecognition;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ActivityLogDB extends SQLiteOpenHelper {
	public static final String TABLE_ACTIVITY = "activities";
	public static final String COLUMN_ACTIVITY_ID = "_id";
	public static final String COLUMN_ACTIVITY_TYPE = "activity";
	public static final String COLUMN_ACTIVITY_TIME = "time";
	public static final String COLUMN_ACTIVITY_CONFIDENCE = "confidence";
	public static final String COLUMN_ACTIVITY_ALTERNATIVES = "alternatives";
	private static final String DB_NAME = "events.db";
	private static final String[] CREATE_DATABASE_STATEMENTS = { "CREATE TABLE " + TABLE_ACTIVITY + " ("
			+ COLUMN_ACTIVITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_ACTIVITY_TYPE
			+ " VARCHAR NOT NULL, " + COLUMN_ACTIVITY_TIME + " INTEGER NOT NULL, " + COLUMN_ACTIVITY_CONFIDENCE
			+ " INTEGER NOT NULL, " + COLUMN_ACTIVITY_ALTERNATIVES + " VARCHAR DEFAULT NULL);" };
	public static final String[] PROJECTION = { COLUMN_ACTIVITY_ID, COLUMN_ACTIVITY_TYPE, COLUMN_ACTIVITY_TIME,
			COLUMN_ACTIVITY_ALTERNATIVES, COLUMN_ACTIVITY_CONFIDENCE };

	public ActivityLogDB(Context context) {
		super(context, DB_NAME, null, CREATE_DATABASE_STATEMENTS.length);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		for (String sql : CREATE_DATABASE_STATEMENTS)
			db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		for (int i = oldVersion; i < newVersion; i++)
			db.execSQL(CREATE_DATABASE_STATEMENTS[i]);
	}
}
