package com.app_it_up.activityrecognition;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class ErrorDialogFragment extends DialogFragment {
	private Dialog dialog;

	public static ErrorDialogFragment create(Dialog dialog) {
		ErrorDialogFragment result = new ErrorDialogFragment();
		result.setDialog(dialog);
		return result;
	}

	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return dialog;
	}
}