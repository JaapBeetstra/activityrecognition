package com.app_it_up.activityrecognition;

import java.text.DateFormat;
import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

final class ActivityAdapter extends ResourceCursorAdapter {
	DateFormat dateFormat = DateFormat.getDateInstance();
	DateFormat timeFormat = DateFormat.getTimeInstance();

	ActivityAdapter(Context context) {
		super(context, R.layout.activity_item, null, 0);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		long time = cursor.getLong(cursor.getColumnIndexOrThrow(ActivityLogDB.COLUMN_ACTIVITY_TIME));
		String type = cursor.getString(cursor.getColumnIndexOrThrow(ActivityLogDB.COLUMN_ACTIVITY_TYPE));
		int confidence = cursor.getInt(cursor.getColumnIndexOrThrow(ActivityLogDB.COLUMN_ACTIVITY_CONFIDENCE));
		String alternatives = cursor
				.getString(cursor.getColumnIndexOrThrow(ActivityLogDB.COLUMN_ACTIVITY_ALTERNATIVES));

		TextView timeView = (TextView) view.findViewById(R.id.activity_time);
		timeView.setText(timeFormat.format(new Date(time)));
		TextView dateView = (TextView) view.findViewById(R.id.activity_date);
		dateView.setText(dateFormat.format(new Date(time)));
		TextView typeView = (TextView) view.findViewById(R.id.activity_type);
		typeView.setText(type);
		TextView confidenceView = (TextView) view.findViewById(R.id.activity_confidence);
		confidenceView.setText(confidence + "%");
		TextView alternativesView = (TextView) view.findViewById(R.id.activity_alternatives);
		alternativesView.setText(alternatives);
	}
}